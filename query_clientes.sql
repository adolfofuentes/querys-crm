SELECT EnjoyWin
FROM [IMC_DM]..[Vector_Clientes]




--- QUERY DM_BSE_CLIENTES & ACTIVOS ----

SELECT ACTIVOS.*, VC.*
FROM (	
		SELECT PLAYER_ID
		FROM IMERCADO..TAFD_RESUMEN T
		WHERE
		T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
		AND (T.SLOT+T.MESA+T.BINGO) > 0
		AND T.CASINO <> 'MENDOZA'
		GROUP BY
		PLAYER_ID
	) ACTIVOS
JOIN (
		SELECT  
		ID_MCC, 
		RUT,
		NOMBRE, 
		APELLIDO_PATERNO, 
		APELLIDO_MATERNO, 
		GENERO, 
		EDAD,
		ESTADO_CIVIL,
		RANGO_ETARIO, 
		COMUNA, REGION, 
		CATEGORIA_TARJETA, 
		SEGMENTO_MOTIVACIONAL , 
		SEGMENTO_VALOR, 
		CASINO_CLIENTE, 
		SEGMENTO_VENDETTA,
		RANGO_DISTANCIA,
		SEGMENTO_FRECUENCIA,
		SEGMENTO_WXV
		FROM [imercado].[dbo].[DM_BSE_CLIENTES] 
		WHERE CARTERA_VIGENTE = 'S' ) VC 
ON ACTIVOS.PLAYER_ID=VC.ID_MCC


------------------ QUERY BASE RODRIGO ---------------

SELECT  DISTINCT (ID_MCC) , GENERO, CATEGORIA_REAL, CASINO_CLIENTE, SEGMENTO_VENDETTA 
FROM IMERCADO..DM_BSE_CLIENTES C
JOIN IMERCADO..TAFD_RESUMEN T ON C.ID_MCC= T.PLAYER_ID
WHERE 
T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
AND (T.SLOT+T.MESA+T.BINGO) > 0
AND CASINO <> 'MENDOZA'
AND C.CARTERA_VIGENTE = 'S'


-------------- QUERY BASE POR CASINO

SELECT  VC.CASINO_CLIENTE, COUNT(VC.CASINO_CLIENTE) AS Q
FROM (	
		SELECT PLAYER_ID
		FROM IMERCADO..TAFD_RESUMEN T
		WHERE
		T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
		AND (T.SLOT+T.MESA+T.BINGO) > 0
		AND T.CASINO <> 'MENDOZA'
		GROUP BY
		PLAYER_ID
	) ACTIVOS
JOIN (
		SELECT  
		ID_MCC, 
		CASINO_CLIENTE
		FROM [imercado].[dbo].[DM_BSE_CLIENTES] 
		WHERE CARTERA_VIGENTE = 'S' ) VC 
ON ACTIVOS.PLAYER_ID=VC.ID_MCC
GROUP BY VC.CASINO_CLIENTE

-------------- QUERY BASE POR SEGMENTO

SELECT  SEGMENTO_VALOR, CATEGORIA_TARJETA , COUNT(*) AS Q
FROM (	
		SELECT PLAYER_ID
		FROM IMERCADO..TAFD_RESUMEN T
		WHERE
		T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
		AND (T.SLOT+T.MESA+T.BINGO) > 0
		AND T.CASINO <> 'MENDOZA'
		GROUP BY
		PLAYER_ID
	) ACTIVOS
JOIN (
		SELECT  
		ID_MCC, 
		SEGMENTO_VALOR,
		CATEGORIA_TARJETA
		FROM [imercado].[dbo].[DM_BSE_CLIENTES] 
		WHERE CARTERA_VIGENTE = 'S' ) VC 
ON ACTIVOS.PLAYER_ID=VC.ID_MCC
GROUP BY VC.SEGMENTO_VALOR , VC.CATEGORIA_TARJETA

------------------ QUERY BASE CON CANTIDAD CON VISITAS Y TICKET PROMEDIO ---------------

SELECT  VC.ID_MCC, VC.SEGMENTO_VALOR, ACTIVOS.*
FROM (	
		SELECT PLAYER_ID, COUNT(PLAYER_ID) AS Q , SUM(SLOT_NWIN) WINSLOT , SUM(MESA_NWIN) WINMESA,  SUM(SLOT_NWIN)+SUM(MESA_NWIN) WINTOTAL
		FROM IMERCADO..TAFD_RESUMEN T
		WHERE
		T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
		AND (T.SLOT+T.MESA+T.BINGO) > 0
		AND T.CASINO <> 'MENDOZA'
		GROUP BY
		PLAYER_ID
	) ACTIVOS
JOIN (
		SELECT  
		ID_MCC, 
		SEGMENTO_VALOR,
		CATEGORIA_TARJETA
		FROM [imercado].[dbo].[DM_BSE_CLIENTES] 
		WHERE CARTERA_VIGENTE = 'S' ) VC 
ON ACTIVOS.PLAYER_ID=VC.ID_MCC


------------------ QUERY SIMPLE CON CANTIDAD CON VISITAS Y TICKET PROMEDIO ---------------


Select  player_id, COUNT(player_id) AS Q , sum(Slot_NWin) WINSLOT , sum(Mesa_NWin) WINMESA,  sum(Slot_NWin)+sum(Mesa_NWin) WINTOTAL 
From imercado..DM_BSE_CLIENTES C
JOIN imercado..tafd_resumen T ON C.ID_MCC= T.player_id
WHERE 
T.Gamingdate between '2019-01-01' and '2019-12-31'
and (T.Slot+T.Mesa+T.Bingo) > 0
and CASINO <> 'Mendoza'
and C.CARTERA_VIGENTE = 'S'
GROUP BY player_id


------------------  QUERY COMPONENTES CLTV ----

SELECT  VC.CATEGORIA_TARJETA , COUNT(VC.CATEGORIA_TARJETA) AS CANTIDAD , AVG(ACTIVOS.Q) AS FREQ, CAST (AVG (ACTIVOS.PROMEDIO) AS INT) AS WIN
FROM (	
		SELECT PLAYER_ID, COUNT(PLAYER_ID) AS Q, SUM(SLOT_NWIN) AS SLOT, SUM(MESA_NWIN) AS MESA , SUM(SLOT_NWIN)+SUM(MESA_NWIN) AS TOT_NWIN, (SUM(SLOT_NWIN)+SUM(MESA_NWIN))/COUNT(PLAYER_ID) AS  PROMEDIO
		FROM IMERCADO..TAFD_RESUMEN T
		WHERE
		T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
		AND (T.SLOT+T.MESA+T.BINGO) > 0
		AND T.CASINO <> 'MENDOZA'
		GROUP BY
		PLAYER_ID) ACTIVOS
INNER JOIN (
			SELECT  
			ID_MCC, 		
    		CATEGORIA_TARJETA
			FROM [IMERCADO].[DBO].[DM_BSE_CLIENTES] 
			WHERE CARTERA_VIGENTE = 'S') VC 
		ON ACTIVOS.PLAYER_ID=VC.ID_MCC
GROUP BY VC.CATEGORIA_TARJETA


------------------  QUERY KPI FUGA  ------------------
   
SELECT BBDD2018.* , BBDD2019.PUNTOS2019 , 
CASE 
	WHEN BBDD2019.CATEGORIA2019 IS NULL THEN '6 FUGA' 
	ELSE BBDD2019.CATEGORIA2019 END AS CATEGORIA2019
FROM
	(		SELECT C.ID_MCC,SUM(PUNTOS) AS PUNTOS2018,
			CASE
				WHEN SUM(PUNTOS) < 15000 THEN '5 SILVER' 
				WHEN SUM(PUNTOS) < 80000 AND SUM(PUNTOS) >= 15000  THEN '4 GOLD' 
				WHEN SUM(PUNTOS) < 250000 AND SUM(PUNTOS) >= 80000 THEN '3 PLATINUM' 
				WHEN SUM(PUNTOS) < 1500000 AND SUM(PUNTOS) >= 250000 THEN '2 DIAMOND' 
				WHEN SUM(PUNTOS) >= 1500000  THEN '1 SEVEN STARS' 
			END AS CATEGORIA2018
    		FROM IMERCADO..DM_BSE_CLIENTES C
    		JOIN IMERCADO..TAFD_RESUMEN T ON C.ID_MCC = T.PLAYER_ID
    		WHERE
    			T.GAMINGDATE BETWEEN '2018-01-01' AND '2018-12-31'
				AND (T.SLOT+T.MESA+T.BINGO) > 0
				AND CASINO <> 'MENDOZA'
				AND C.CARTERA_VIGENTE = 'S'
    		GROUP BY
    		C.ID_MCC) BBDD2018
LEFT JOIN ( 
			SELECT C.ID_MCC,SUM(PUNTOS) AS PUNTOS2019,
			CASE
				WHEN SUM(PUNTOS) < 15000 THEN '5 SILVER' 
				WHEN SUM(PUNTOS) < 80000 AND SUM(PUNTOS) >= 15000  THEN '4 GOLD' 
				WHEN SUM(PUNTOS) < 250000 AND SUM(PUNTOS) >= 80000 THEN '3 PLATINUM' 
				WHEN SUM(PUNTOS) < 1500000 AND SUM(PUNTOS) >= 250000 THEN '2 DIAMOND' 
				WHEN SUM(PUNTOS) >= 1500000  THEN '1 SEVEN STARS' 
			END AS CATEGORIA2019 
    		FROM IMERCADO..DM_BSE_CLIENTES C
    		JOIN IMERCADO..TAFD_RESUMEN T ON C.ID_MCC = T.PLAYER_ID
    		WHERE
    			T.GAMINGDATE BETWEEN '2019-01-01' AND '2019-12-31'
				AND (T.SLOT+T.MESA+T.BINGO) > 0
				AND CASINO <> 'MENDOZA'
				AND C.CARTERA_VIGENTE = 'S'
    		GROUP BY
    		C.ID_MCC) BBDD2019
			ON BBDD2018.ID_MCC=BBDD2019.ID_MCC




------------------ Periodo de Cuarentena Sin Juego  ---------------

Declare @cuarentena_start as date, @cuarentena_end as date, @fecha_inicio as date,  @fecha_fin as date, @fecha_inicio_OG as date
  
Set @cuarentena_start = '2020-03-18' --primer dia de cuarentena
Set @cuarentena_end = '2020-11-18'   --ultimo dia de cuarentena
Set @fecha_inicio_OG = '2019-01-14'  --fecha inicial, definida por el usuario
Set @fecha_fin = '2021-01-14'        --fecha final, definida por el usuario

  
Set @fecha_inicio = CASE  
WHEN (@fecha_inicio_OG > @cuarentena_end) or (@fecha_fin < @cuarentena_start) THEN @fecha_inicio_OG  --no hay correccion
WHEN (@fecha_inicio_OG >= @cuarentena_start) and (@fecha_inicio_OG <= @cuarentena_end)
THEN DATEADD(day, -DATEDIFF(day, @fecha_inicio_OG, @cuarentena_end) - 1, @cuarentena_start) --fecha inicio cae dentro de cuarentena
ELSE DATEADD(day, -246, @fecha_inicio_OG) END  --hay que corregir por la cuarentena completa

SELECT player_id, SUM(Slot + Mesa) as Puntos
       FROM imercado..tafd_resumen tr
       WHERE
       tr.Gamingdate between @fecha_inicio and @fecha_fin
       and tr.Gamingdate not between @cuarentena_start and @cuarentena_end
       and (Slot > 0 or Mesa > 0 or Bingo > 0)
and CASINO != 'Mendoza'
and player_id in (
SELECT  ID_MCC
FROM [imercado].[dbo].[DM_BSE_CLIENTES]
WHERE ESTADO_CLIENTE in ('Normal','Miembro de honor'))
GROUP BY player_id
ORDER BY player_id


--Categoria_real -> Categoria real en base a Puntos
--Categoria_tarjeta  -> Plastico
--Segmento_valor -> Segmentacion CRM
--Casino_Cliente -> se evalua 1 vez al a�o, donde se apertura la tarjeta y luego frecuencia de compra temas comunicacionales

----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------

